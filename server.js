const express = require('express')
const app = express()
const contacto = require('./contacto.json')
const fs = require('fs')
// const hbs = require('hbs')
app.set('view engine', 'hbs'); // clave valor
const path = require('path')
const staticRoute = path.join(__dirname, 'public')

app.use(express.static(staticRoute))

app.use(function (req, res, next)  {
    var now = new Date().toString()
    var log = `${now}: ${req.method} ${req.url}`
    fs.appendFile('log.txt', `${log}\n`, (err) => {
        if (err) console.log(`No se ha podido usar el fichero de log:  ${err}`)
      })
      next()
})
app.use(function (req, res, next) {
    var now = new Date().toString()
    console.log(`Time: ${now} ${req.method} ${req.url}`)
    next()
  })
app.get('/', (req, res) => {
    res.send('Hola Mundo!!!!!')
})
app.get('/contactar', (req, res) => {
    res.send('Página para contactar')
})
app.get('/contacto', (req, res) => {
    res.send(contacto)
})
app.listen(3000,() =>{
    console.log('Servidor web arrancado')
})